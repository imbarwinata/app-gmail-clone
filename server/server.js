const path = require('path');
const express = require('express');
const app = express();
const listEndpoints = require("express-list-endpoints");
const publicPath = path.join(__dirname, '..', 'public');
const port = process.env.PORT || 3000;
// console.log = function(){}; // disable log in server

const authRouter = require('./routes/auth');
const mailRouter = require('./routes/mails');
const personRouter = require('./routes/persons');

// NOTE: settings for throwing to frontoffice
//     : failed to load No 'Access-Control-Allow-Origin' header is present
// app.use(function(req, res, next){
//   res.header('Access-Control-Allow-Origin', "*");
//   res.header('Access-Control-Allow-Methods', "GET, PATCH, POST, DELETE");
//   res.header('Access-Control-Allow-Headers', "Content-Type");
//   next();
// });

app.use(express.static(publicPath));
app.use(authRouter);
app.use(mailRouter);
app.use(personRouter);

app.get('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'));
});

app.listen(port, () => {
  console.log('Server is up!');
});