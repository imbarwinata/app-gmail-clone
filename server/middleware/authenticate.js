var jwt = require("jsonwebtoken");
var config = require("./../config/config");
var getSession = require("./../config/sessions");
var localStorage = require('localStorage');

function checkToken(req, res, next) {
  var token =
    localStorage.getItem('token') != null
      ? localStorage.getItem('token')
      : req.headers["x-access-token"];
  if (!token)
    return res.status(403).send({ auth: false, message: "No token provided." });
  jwt.verify(token, config.secret, function(err, decoded) {
    if (err)
      return res
        .status(500)
        .send({ auth: false, message: "Failed to authenticate token." });
    // if everything good, save to request for use in other routes
    req.userId = decoded.id;
    next();
  });
}

module.exports = checkToken;