const express = require("express");
const listEndpoints = require("express-list-endpoints");
const bodyParser = require("body-parser");
// custom
const app = express();
const models = require("./../../src/models");
const checkToken = require("./../middleware/authenticate");
var moment = require('moment-timezone');
var request = require('request');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.post("/api/persons/register", (req, res) => {
    let person = {...req.body};
    let dataPerson = person.dataPerson;
    
    models.Person.create(dataPerson).then(result => { 
        res.send({
            message: "Registration Success.",
            status: "success"
        });
    }).catch(err => {
        res.send({
            message: "Registration Failed.",
            status: "error"
        });
    });
});
app.get("/api/persons/check/:p_email", (req, res) => {
    let p_email = req.params.p_email;
    models.Person.findOne({ where: { p_email } }).then(persons => {
        if(persons){
            res.send({
                'status': 'error',
                'message': 'Email is already in use by others.'
            });
        }else{
            res.send({
                'status': 'success',
                'message': 'Email is unused by others.'
            });
        }
    }).catch(err => {
        res.send({'status':'error', 'message': err.original.sql});
    });
});
// app.get("/api/persons", (req, res) => {
//     models.Person.findAll().then(persons => {
//         res.send({
//             'status': 'success',
//             'data': persons
//         });
//     }).catch(err => {
//         res.send({'status':'error', 'message': err.original.sql});
//     });
// });

console.log(listEndpoints(app));
module.exports = app;