const express = require("express");
const listEndpoints = require("express-list-endpoints");
const bodyParser = require("body-parser");
// custom
const app = express();
const models = require("./../../src/models");
const checkToken = require("./../middleware/authenticate");
var moment = require('moment-timezone');
var request = require('request');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.get("/api/mails/check-exist/:email", checkToken, (req, res) => {
  p_email = req.params.email;
  const dataMails = [];
  models.Person.findOne({ where: {p_email} }).then(mails => {
    if(mails){
      res.send({
        'status': 'success',
        'message': '',
        'data': mails
      });
    }
    else{
      res.send({
        'status': 'failed',
        'message': 'email not found!',
        'data': mails
      });
    }
  }).catch(err => {
    res.send({'status':'error', 'message': err.original.sql});
  });
});


app.patch("/api/mails/read/:mail_id", checkToken, (req, res) => {
  mail_id = req.params.mail_id;
  models.Mail.update({mail_read: "t"}, {
    where: { mail_id },
    returning: true,
    plain: true
  }).then(data => {
    res.send({
      message: "Email berhasil diperbaharui.",
      status: "success",
      data
    });
  }).catch(err => {
    res.send({
      message: "Email gagal diperbaharui.",
      status: "error"
    });
  });

});

app.post("/api/mails/compose", checkToken, (req, res) => {
  let mail = { ...req.body};
  let dataMail = mail.dataMail;

  models.Mail.create(dataMail).then(resultMail => {
    res.status(200).send({
      message: "Email berhasil dikirim.",
      data: resultMail,
      status: "success"
    });
  }).catch(err => {
    res.send({
      message: "Email gagal dikirim.",
      status: "error"
    });
  });

});

app.get("/api/mails/inbox/:email", checkToken, (req, res) => {
  email = req.params.email;
  const dataMails = [];
  models.Mail.findAll({ where: {mail_to: email} }).then(mails => {
    mails.forEach((dataMail) => {
      dataMails.push({
        mail_id: dataMail.mail_id,
        mail_subject: dataMail.mail_subject,
        mail_from: dataMail.mail_from,
        mail_to: dataMail.mail_to,
        mail_message: dataMail.mail_message,
        mail_read: dataMail.mail_read,
        createdAt: dataMail.createdAt
      });
    });
    res.send({
      'status': 'success',
      'data': dataMails
    });
  }).catch(err => {
    res.send({'status':'error', 'message': err.original.sql});
  });
});

app.get("/api/mails/sending/:email", checkToken, (req, res) => {
  email = req.params.email;
  const dataMails = [];
  models.Mail.findAll({ where: {mail_from: email} }).then(mails => {
    mails.forEach((dataMail) => {
      dataMails.push({
        mail_id: dataMail.mail_id,
        mail_subject: dataMail.mail_subject,
        mail_from: dataMail.mail_from,
        mail_to: dataMail.mail_to,
        mail_message: dataMail.mail_message,
        mail_read: dataMail.mail_read,
        createdAt: dataMail.createdAt
      });
    });
    res.send({
      'status': 'success',
      'data': dataMails
    });
  }).catch(err => {
    res.send({'status':'error', 'message': err.original.sql});
  });
});

console.log(listEndpoints(app));
module.exports = app;