const listEndpoints = require("express-list-endpoints");
const express = require("express");
const app = express();
const models = require("./../../src/models");
const bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
var config = require("./../config/config");
var checkToken = require("./../middleware/authenticate");
var localStorage = require("localStorage");
let randomInt = require('random-int');
let md5 = require('md5');
const mailgun_api_key = require("./../config/config").mailgun_api_key;
const mailgun_domain = require("./../config/config").mailgun_domain;
const mail_default = require("./../config/config").mail_default;
const mailgun = require('mailgun-js')({apiKey: mailgun_api_key, domain: mailgun_domain});

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true
  })
);

app.patch("/api/change-password", checkToken, (req, res) => {
  let p_id = req.body.where.p_id;
  let p_email = req.body.where.p_email;
  let p_pass = req.body.where.p_pass;
  let dataAdmin = req.body.dataAdmin;

  models.Person.findOne({
    where: { p_id, p_email, p_pass }
  }).then(admin => {
    if(admin === null){
      res.send(config.messageInfo("Failed", "Password tidak sama!"));      
    }
    else{
      models.Person.update(dataAdmin, {
        where: { p_id, p_email, p_pass },
        returning: true,
        plain: true
      }).then(result => {
        res.send(config.messageInfo("OK", "Password berhasil diperbaharui."));
      }).catch(err => {
        res.send(config.messageInfo("Failed", "Password gagal diperbaharui!"));
      }); 
    }
  }).catch(err => {
    res.send({ 
      status: "Failed",
      message: 'Password tidak sama!'
    });
  });
});

app.post("/api/auth/login", (req, res) => {
  let data = req.body;

  models.Person.findOne({
    where: {
      p_email: data.email,
      p_pass: data.password
    }
  })
    .then(result => {
      var token = jwt.sign(
        { id: result.p_id, email: result.p_email, name: result.p_name },
        config.secret,
        {
          expiresIn: (60 * 60) * 7 // expires in 24 hours * 7 day
        }
      );
      localStorage.setItem("auth", true);
      localStorage.setItem("token", token);

      res
        .status(200)
        .send({
          auth: localStorage.getItem("auth"),
          token: localStorage.getItem("token"),
          id: result.p_id,
          name: result.p_name,
          email: result.p_email
        });
    })
    .catch(err => {
      res.send({ auth: false, token: null });
    });
});

app.get("/api/auth/logout", function(req, res) {
  localStorage.removeItem("auth");
  localStorage.removeItem("token");
  res.status(200).send({ auth: false, token: null });
});

app.post("/api/auth/checkToken", (req, res) => {
  var token = req.body.token;
  if (!token) return res.send({ auth: false, token: null });
  jwt.verify(token, config.secret, function(err, decoded) {
    if (err) return res.send({ auth: false, token: null });

    res.send({ auth: true, token: token });
  });
});

console.log(listEndpoints(app));
module.exports = app;
