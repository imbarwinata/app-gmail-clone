const linkFacebook = require('./../config/config').linkFacebook;
const linkTwitter = require('./../config/config').linkTwitter;
const owner = require('./../config/config').owner;
const email = require('./../config/config').mail_contact;

module.exports = {
  register: function(name) {
    let template = `
    <div bgcolor="#eeeeee" style="font-family:Arial,Helvetica,sans-serif">
        <table align="center" height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
            <tbody>
                <tr>
                    <td>
                        <table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
                            <tbody>
                                <tr>
                                    <td colspan="4" height="110" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="padding:0;margin:0;font-size:1;line-height:0">
                                        <table width="690" align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="20"></td>
                                                    <td align="left" valign="middle" style="padding:0;margin:0;font-size:1;line-height:0">
                                                        <img src="http://sahabat-tani.website/images/logo-1.png"
                                                            alt="sahabat-tani" class="CToWUd" style="width: 300px; margin-left: 160px;">
                                                    </td>
                                                    <td width="20"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr bgcolor="#242a33">
                                    <td colspan="4" align="center">
                                        <table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr></tr>
                                                <tr>
                                                    <td width="30"></td>
                                                    <td align="center"></td>
                                                    <td width="30"></td>
                                                </tr>
                                                <tr></tr>
                                                <tr></tr>
                                                <tr></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr></tr>
                                <tr bgcolor="#ffffff">
                                    <td width="30"></td>
                                    <td>
                                        <table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td colspan="4" width="630" height="50" style="padding:0;margin:0;font-size:1;line-height:0"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="padding:0;margin:0;font-size:1;line-height:0">
                                                        <h2 style="color:#404040;font-size:22px;font-weight:bold;line-height:26px;padding:0;margin:0">Hi ${name},</h2>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="20"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div style="color:#404040;font-size:16px;line-height:20px;padding:0;margin:0">
                                                            <span class="il">Pemesanan anda sudah sampai di kota tujuan anda dan sudah diterima.</span></div>
                                                        <div style="color:#404040;font-size:16px;line-height:20px;padding:0;margin:0">&nbsp;</div>
                                                        <div style="color:#404040;font-size:16px;line-height:20px;padding:0;margin:0">
                                                            Terimakasih sudah berbelanja di toko kami, semoga pelayanan yang kami berikan bisa menyenangkan anda.
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="30"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4"></td>
                                                </tr>
                                                <tr></tr>
                                                <tr>
                                                    <td align="left"></td>
                                                </tr>
                                                <tr></tr>
                                                <tr>
                                                    <td width="150" align="right"></td>
                                                </tr>
                                                <tr></tr>
                                                <tr>
                                                    <td width="30"></td>
                                                    <td width="50"></td>
                                                </tr>
                                                <tr></tr>
                                                <tr></tr>
                                                <tr></tr>
                                                <tr>
                                                    <td colspan="4"></td>
                                                </tr>
                                                <tr></tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <div style="color:#404040;font-size:18px;line-height:20px;padding:0;margin:0">Regards,</div>
                                                        <div style="color:#404040;font-size:18px;line-height:20px;padding:0px 0px 40px 0px;margin:0">${owner}.</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="30" bgcolor="#ffffff"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="30"></td>
                </tr>
                <tr>
                    <td>
                        <table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <img src="http://sahabat-tani.website/images/logo-footer.png"
                                            alt="----------------------------------" class="CToWUd">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="5"></td>
                                                    <td>
                                                        <a href="${linkFacebook}" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=${linkFacebook}&amp;source=gmail&amp;ust=1527004190490000&amp;usg=AFQjCNHOSpP1uybREDVYveqrT1xSeeEl5g">
                                                            <img src="https://ci4.googleusercontent.com/proxy/A54GRrneo_f4j1PNZBt_uqyv2ovn5AhFD1cTbxvjvHCDG8ujLrfWqcED1GLAi2jwt4KJPEhzxrRpLyqvhHW6Yk9so7rlH5jX-nVYnec4-0QkIjGy606477NWxoyWVtev5O4HeUB2YakTbjpExVhU3du_=s0-d-e1-ft#https://cdn3.f-cdn.com/img/email-images/social-fb.png?v=5aa6e499ff7dc71c191e2939b4b94ab3&amp;m=6"
                                                                alt="fb" class="CToWUd">
                                                        </a>
                                                    </td>
                                                    <td width="15"></td>
                                                    <td>
                                                        <a href="${linkTwitter}" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=${linkTwitter}&amp;source=gmail&amp;ust=1527004190490000&amp;usg=AFQjCNGIZSTpKPPqZFTqvPULwGxRGyIx2A">
                                                            <img src="https://ci4.googleusercontent.com/proxy/ynqjbTxeHx8uLt7Y3OntRHiTqewA3SSSZbBJpwwwmSYWh8laePSmFDWgVthpNLzi61ortgqgZ11tkAT_uObaXy47NFaRhoJjJ4mqHvJ0mJqJB_a7viDx7vKEsQokf-8WSPuOG7mcg3DYwUhGVNLAEtNPC-s=s0-d-e1-ft#https://cdn2.f-cdn.com/img/email-images/social-twit.png?v=3f51007449eeaabf8588cf3da3fdd956&amp;m=6"
                                                                alt="twit" class="CToWUd">
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" height="5"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <div style="color:#bebebe;font-size:12px;line-height:12px;padding:0;margin:0 0 10px 0">© 2018 Sahabat Tani Ciapus | All Rights Reserved</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <div style="color:#bebebe;font-size:12px;line-height:12px;padding:0;margin:0 0 10px 0">Jika anda punya pertanyaan, kontak kami di
                                            <a href="mailto:${email}" style="color:#bebebe;text-decoration:underline" target="_blank">${email}</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="https://ci4.googleusercontent.com/proxy/jJE2UjbDsL7kBfBWELW5IzsMtPmTpjW20IllVWXykQo_YwDzLNZWPRcbQB5NnVN_JY7SlHrYpKaAfj04ibOdXwSU4dvBLVkQh_TH7Oc27Duo0m9abl9yK92EKbiEn8HCv3TtJhIEBssTNfiGr1RW7wNhxAALOAslqmusI6qKZmMYo2UdYlw6VMMYCKyj7oqewmpqlgpUZmyZfEKqY7mnl6JQadnlXjKGr10GnMa1xbS0=s0-d-e1-ft#https://t.freelancer.com/1px.gif?user_id=&amp;en=email_open&amp;program=MEQ&amp;program_id=1001X&amp;method=img&amp;acct=.Freelancer.com&amp;uniqid=21209387-628-57d7ab55-700a5a5a"
                                            style="border:1px solid #d9d9d9" alt="" class="CToWUd">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    `;
    return template;
  }
};
