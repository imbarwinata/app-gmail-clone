const moment = require('moment');
const uuid = require('uuid');

module.exports = {
    profile: function (adm_id, labelProfil) {
        let dataProfile = {
            id: null,
            po_id: uuid(),
            adm_id: adm_id,
            po_judul: null,
            po_isi: null,
            po_tanggal: moment().format('YYYY-MM-DD HH:mm:ss'),
            po_gambar: null,
            po_lihat: null,
            po_faq: null,
            po_halaman: labelProfil
        }
        return dataProfile;
    }
};