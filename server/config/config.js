const fs = require("fs-extra");
const path = require("path");
const createHTML = require("create-html");
let host = "sahabat-tani.website";
// let host = "localhost";

module.exports = {
  owner: "Pemilik Sahabat Tani",
  secret: "secretimbar",
  secretFront: "secretwinata",
  mail_contact: "sahabattani@gmail.com",
  mail_default: "admin@sahabattani.com",
  mailgun_api_key: "key-4e21ff34d1401b855827d6ff4144abab",
  mailgun_domain: "imbarwinata.tech",
  linkFacebook: "http://facebook.com/imbar.winata",
  linkTwitter: "http://twitter.com/imbar_winata",
  linkFront: host == 'localhost' ? `http://${host}:3333`:`http://${host}`,
  pathUpload: function(includePath) {
    let pathname = path.join(
      __dirname,
      "../../public/uploads/",
      `${includePath}`
    );
    var html = createHTML({
      title: "Can't Access",
      lang: "en",
      body: "<p>Sorry, you can't access this page.</p>"
    });
    try {
      fs.writeFile(pathname + "/index.html", html);
    } catch (e) {
      console.log("Cannot write file ", e);
    }
    return pathname;
  },
  messageInfo: function(status, message){
    let messageData = { status, message };
    return messageData;
  }
};