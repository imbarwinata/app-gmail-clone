var localStorage = require('localStorage');

exports.getSession = {
    backoffice: {
        auth: localStorage.getItem('auth'),
        token: localStorage.getItem('token')
    }
}