import React from 'react';
import ReactDOM from 'react-dom';

export const loadLink = (id='',rel='',type='',href='') => {
    var tag     = document.createElement('link');
    tag.async   = false;
    tag.rel     = rel;
    tag.type    = type;
    tag.href    = href;
    document.getElementById(id).append(tag);
};

export const loadScript = (id='',src='') => {
    var tag     = document.createElement('script');
    tag.async   = false;
    tag.src     = src;
    document.getElementById(id).append(tag);
};

export const loadImage = () => {
    
};