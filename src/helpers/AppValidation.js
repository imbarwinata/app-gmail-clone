exports.isMax = (field = '') => {
    return `The number of ${field} characters exceeds the maximum limit`;
}

exports.isMin = (field = '') => {
    return `The number of ${field} characters exceeds the minimum limit`;
}

exports.isRequired = (field = '') => {
    return `${field} is required.`;
}