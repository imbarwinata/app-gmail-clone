import path from "path";

exports.getSession = {
  backoffice: {
    auth: localStorage.getItem("auth") ? localStorage.getItem("auth") : null,
    token: localStorage.getItem("token") ? localStorage.getItem("token") : null,
    folderUpload: function(includePath) {
      let pathname = path.join( __dirname, "/uploads/", `${includePath}`);
      
      return pathname;
    }  
  }
};