import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import mailInboxsReducer from '../reducers/mailInboxs';
import mailSentsReducer from '../reducers/mailSents';
import authReducer from '../reducers/auth';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ = for enable devTools redux in extension

export default () => {
  const store = createStore(
    combineReducers({
      mailInboxs: mailInboxsReducer,
      mailSents: mailSentsReducer,
      auth: authReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};