import axios from 'axios';

// ADD_MAILSENTS
export const addMailSent = (mailSent) => ({
  type: 'ADD_MAILSENT',
  mailSent
});

export const startAddMailSent = (mailSentsData = {}) => {
  return (dispatch, getState) => {
    const {
      mail_id = "",
      mail_subject = "",
      mail_from = "",
      mail_to = "",
      mail_message = "",
      createdAt = moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt = 0
    } = mailSentsData;
    
    const mailSents = { 
      dataMail: {
        mail_id, mail_subject, mail_from, mail_to, mail_message, createdAt, updatedAt
      }
    };

    return axios.post(`${window.location.origin}/api/mails/compose`, mailSents).then(res => {
        dispatch(
          addMailSent({
            id: mailSents.mail_id,
            ...mailSents
          })
        );
      })
      .catch(err => {});
  };
};

// SET_MAILSENTS
export const setMailSents = (mailSents) => ({
  type: 'SET_MAILSENTS',
  mailSents
});

export const startSetMailSents = (email) => {
  return (dispatch, getState) => {
    const mailSents = [];

    return axios.get(`${window.location.origin}/api/mails/sending/${email}`).then(res => {
      res.data.data.forEach((dataMailSents) => {
        mailSents.push({
         ...dataMailSents 
        });
      });
      dispatch(setMailSents(mailSents));
    }).catch((err) => {
      
    });
  };
};