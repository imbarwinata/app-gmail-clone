import axios from "axios";
import { history } from "./../routers/AppRouter";
import { config } from "./../helpers/AppConfig";

// ADD_PERSON
export const startAddPerson = (personData = {}) => {
    return (dispatch, getState) => {
        const {
            p_id = '',
            p_name = '',
            p_gender = '',
            p_email = '',
            p_pass = '',
            p_hp = '',
            p_aktif = 't'
        } = personData;
        const person = { 
          dataPerson: { 
            p_id, p_name, p_gender, p_email, p_pass, p_hp, p_aktif 
          }
        };

        return axios.post(`${window.location.origin}/api/persons/register`, person).then(res => {
            alert('Registration is successful, please login to proceed.');
          })
          .catch(err => {});
    };
};