import axios from "axios";
import { history } from "./../routers/AppRouter";

export const login = auth => ({
  type: "LOGIN",
  auth
});

export const startLogin = dataLogin => {
  return (dispatch, getState) => {
    const { email = "", password = "" } = dataLogin;
    const user = { email, password };

    return axios
      .post(`${window.location.origin}/api/auth/login`, user)
      .then(res => {
        if (res.data.token != null) {
          localStorage.setItem("auth", res.data.auth);
          localStorage.setItem("token", res.data.token);
          localStorage.setItem("a_id", res.data.id);
          localStorage.setItem("a_name", res.data.name);
          localStorage.setItem("a_email", res.data.email);
          dispatch(
            login({
              auth: res.data.auth,
              token: res.data.token,
              a_id: res.data.id,
              a_name: res.data.name,
              a_email: res.data.email
            })
          );
          history.push("/mails/inbox");
          alert(`Hai, selamat datang kembali ${res.data.name}`);          
        } else {
          history.push("/");
          alert("email and password tidak sesuai.\nSilahkan ulangi lagi.");
        }
      });
  };
};

export const logout = () => ({
  type: "LOGOUT"
});

export const startLogout = () => {
  localStorage.clear();
  return dispatch => {
    return axios
      .get(`${window.location.origin}/api/auth/logout`)
      .then(res => {
        localStorage.removeItem("auth");
        localStorage.removeItem("token");
        dispatch(logout());
      })
      .catch(err => {});
  };
};

export const startSetLogin = dataToken => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('token'); 
    return axios
      .post(`${window.location.origin}/api/auth/checkToken`, { token })
      .then(res => {
        if (res.data.auth != false || res.data.token != null) {
          dispatch(
            login({
              auth: res.data.auth,
              token: res.data.token
            })
          );
        }
      });
  };
};

export const startChangeDataPasswordAdmin = (passwordData = {}) => {
  return (dispatch, getState) => {
      const {
          adm_id = '',
          adm_namapengguna = '',
          adm_password = '',
          adm_passwordNew = ''
      } = passwordData;
      let dataSend = {
          where:{ adm_id, adm_namapengguna, adm_password },
          dataAdmin:{ adm_password: adm_passwordNew}
      }
      return axios.patch(`${window.location.origin}/api/change-password`, dataSend).then(res => {
          alert(res.data.message);
          if(res.data.status == "OK"){
              history.push('/ubah-password');
          }
      });
  };
};
