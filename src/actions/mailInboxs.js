import axios from 'axios';


// EDIT_MAILINBOX
export const editMailInbox = (id, updates) => ({
  type: 'EDIT_MAILINBOX',
  id,
  updates
});

export const startEditMailInbox = (id, mailSentsData) => {
  return (dispatch, getState) => {
    const {
      updatedAt = moment().format('YYYY-MM-DD HH:mm:ss')
    } = mailSentsData;
    
    const mailSent = { updatedAt };

    return axios.patch(`${window.location.origin}/api/mails/read/${id}`, mailSent).then(res => {
      dispatch(
        editMailInbox({
          id: mailSents.mail_id,
          ...mailSents
        })
      );
      dispatch(editMailInbox(id, mailSent));
    })
    .catch(err => {});
  };
};



// SET_MAILINBOXS
export const setMailInboxs = (mailInboxs) => ({
  type: 'SET_MAILINBOXS',
  mailInboxs
});

export const startSetMailInboxs = (email) => {
  return (dispatch, getState) => {
    const mailInboxs = [];

    return axios.get(`${window.location.origin}/api/mails/inbox/${email}`).then(res => {
      res.data.data.forEach((dataMailInboxs) => {
        mailInboxs.push({
         ...dataMailInboxs 
        });
      });
      dispatch(setMailInboxs(mailInboxs));
    }).catch((err) => {
      
    });
  };
};