import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
  startAddArticle,
  addArticle,
  editArticle,
  startEditArticle,
  removeArticle,
  startRemoveArticle,
  setArticles,
  startSetArticles
} from "../../actions/articles";
import articles from "../fixtures/articles";

const uid = "thisismytestuid";
const defaultAuthState = { auth: { uid } };
const createMockStore = configureMockStore([thunk]);

beforeEach(done => {
  const articlesData = {};
  articles.forEach(({ id, title, subtitle, description, createdAt }) => {
    articlesData[id] = { title, subtitle, description, createdAt };
  });
  database
    .ref(`users/${uid}/articles`)
    .set(articlesData)
    .then(() => done());
});

test("should setup remove article action object", () => {
  const action = removeArticle({ id: "123abc" });
  expect(action).toEqual({
    type: "REMOVE_ARTICLE",
    id: "123abc"
  });
});

test("should remove article from postgres", done => {
  const store = createMockStore(defaultAuthState);
  const id = articles[2].id;
  store
    .dispatch(startRemoveArtremoveArticle({ id }))
    .then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual({
        type: "REMOVE_ARTICLE",
        id
      });
      return database.ref(`users/${uid}/articles/${id}`).once("value");
    })
    .then(snapshot => {
      expect(snapshot.val()).toBeFalsy();
      done();
    });
});

test("should setup edit article action object", () => {
  const action = editArticle("123abc", { subtitle: "New note value" });
  expect(action).toEqual({
    type: "EDIT_ARTICLE",
    id: "123abc",
    updates: {
      subtitle: "New note value"
    }
  });
});

test("should edit article from postgres", done => {
  const store = createMockStore(defaultAuthState);
  const id = articles[0].id;
  const updates = { description: 21045 };
  store
    .dispatch(startEditArticeditArticle(id, updates))
    .then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual({
        type: "EDIT_ARTICLE",
        id,
        updates
      });
      return database.ref(`users/${uid}/articles/${id}`).once("value");
    })
    .then(snapshot => {
      expect(snapshot.val().description).toBe(updates.description);
      done();
    });
});

test("should setup add article action object with provided values", () => {
  const action = addArticle(articles[2]);
  expect(action).toEqual({
    type: "ADD_ARTICLE",
    article: articles[2]
  });
});

test("should add article to database and store", done => {
  const store = createMockStore(defaultAuthState);
  const articleData = {
    title: "Mouse",
    description: 3000,
    subtitle: "This one is better",
    createdAt: 1000
  };

  store
    .dispatch(startAddArticle(articleData))
    .then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual({
        type: "ADD_ARTICLE",
        article: {
          id: expect.any(String),
          ...articleData
        }
      });

      return database
        .ref(`users/${uid}/articles/${actions[0].article.id}`)
        .once("value");
    })
    .then(snapshot => {
      expect(snapshot.val()).toEqual(articleData);
      done();
    });
});

test("should add article with defaults to database and store", done => {
  const store = createMockStore(defaultAuthState);
  const articleDefaults = {
    title: "",
    description: 0,
    subtitle: "",
    createdAt: 0
  };

  store
    .dispatch(startAddArticle({}))
    .then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual({
        type: "ADD_ARTICLE",
        article: {
          id: expect.any(String),
          ...articleDefaults
        }
      });

      return database
        .ref(`users/${uid}/articles/${actions[0].article.id}`)
        .once("value");
    })
    .then(snapshot => {
      expect(snapshot.val()).toEqual(articleDefaults);
      done();
    });
});

test("should setup set article action object with data", () => {
  const action = setArticles(articles);
  expect(action).toEqual({
    type: "SET_ARTICLES",
    articles
  });
});

test("should fetch the articles from postgres", done => {
  const store = createMockStore(defaultAuthState);
  store.dispatch(startSetArticles()).then(() => {
    const actions = store.getActions();
    expect(actions[0]).toEqual({
      type: "SET_ARTICLES",
      articles
    });
    done();
  });
});