import React from "react";
import ReactShallowRenderer from "react-test-renderer/shallow";
import Header from "../../../components/layouts/Header";

test("should render layouts/Header correctly", () => {
  const renderer = new ReactShallowRenderer();
  const props = {
    history: {
      location: {
        pathname: ""
      }
    }
  };
  renderer.render(<Header history={props} />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});
