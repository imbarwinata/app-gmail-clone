import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import AppFooter from '../../../components/layouts/AppFooter';

test('should render layouts/AppFooter correctly', () => {
  const renderer = new ReactShallowRenderer();
  const core = {
    titleHeader: "ini header",
    titleBody: "ini body",
    titleFooter: "ini footer"
  };  
  renderer.render(<AppFooter core={core} />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});