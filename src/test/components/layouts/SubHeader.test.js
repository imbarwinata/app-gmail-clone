import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import SubHeader from '../../../components/layouts/SubHeader';

test('should render layouts/SubHeader correctly', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<SubHeader />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});