import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import AppHeader from '../../../components/layouts/AppHeader';

test('should render layouts/AppHeader correctly', () => {
  const renderer = new ReactShallowRenderer();
  const core = {
    titleHeader: "ini header",
    titleBody: "ini body",
    titleFooter: "ini footer"
  };  
  renderer.render(<AppHeader {...core} />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});