import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import LoadingPage from '../../../components/layouts/LoadingPage';

test('should render layouts/LoadingPage correctly', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<LoadingPage />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});