import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import AppScripts from '../../../components/layouts/AppScripts';

test('should render layouts/AppScripts correctly', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<AppScripts />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});