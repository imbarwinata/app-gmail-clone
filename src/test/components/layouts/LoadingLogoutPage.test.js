import React from "react";
import ReactShallowRenderer from "react-test-renderer/shallow";
import LoadingLogoutPage from "../../../components/layouts/LoadingLogoutPage";

test("should render layouts/LoadingLogoutPage correctly", () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<LoadingLogoutPage />);
  expect(renderer.getRenderOutput()).toMatchSnapshot();
});
