import moment from 'moment'
import uuid from 'uuid';

export default [{
  id: uuid(),
  title: 'Lorem',
  subtitle: 'Lorem Ipsum',
  description: '<p>Test Lorem</p>',
  createdAt: 0
}, {
  id: '2',
  title: 'Ipsum',
  subtitle: 'Lorem Ipsum Sir',
  description: '<p>Test Ipsum</p>',
  createdAt: moment(0).subtract(4, 'days').valueOf()
}, {
  id: '3',
  title: 'Lorem Ipsum',
  subtitle: 'Lorem Ipsum Color Dumet',
  description: '<p>Test Lorem Ipsum</p>',
  createdAt: moment(0).add(4, 'days').valueOf()
}];