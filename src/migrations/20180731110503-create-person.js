'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('People', {
      id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      p_id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING(40)
      },
      p_name: {
        allowNull: true,
        type: Sequelize.STRING(50)
      },
      p_gender: {
        allowNull: true,
        type: Sequelize.STRING(1)
      },
      p_email: {
        allowNull: true,
        type: Sequelize.STRING(40)
      },
      p_pass: {
        allowNull: true,
        type: Sequelize.STRING(40)
      },
      p_hp: {
        allowNull: true,
        type: Sequelize.STRING(13)
      },
      p_aktif: {
        allowNull: true,
        type: Sequelize.STRING(1)
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('People');
  }
};