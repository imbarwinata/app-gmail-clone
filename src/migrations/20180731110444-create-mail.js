'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Mails', {
      id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      mail_id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING(40)
      },
      mail_subject: {
        allowNull: false,
        type: Sequelize.STRING(78)
      },
      mail_from: {
        allowNull: false,
        type: Sequelize.STRING(254)
      },
      mail_to: {
        allowNull: false,
        type: Sequelize.STRING(254)
      },
      mail_message: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      mail_read: {
        allowNull: false,
        defaultValue: 'f',
        type: Sequelize.STRING(1)
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Mails');
  }
};