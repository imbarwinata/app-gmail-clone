// MailInboxs Reducer

const mailInboxsReducerDefaultState = [];

export default (state = mailInboxsReducerDefaultState, action) => {
  switch (action.type) {
    case 'SET_MAILINBOXS':
      return action.mailInboxs;
    case 'EDIT_MAILINBOX':
      return state.map((mailInbox) => {
        if (mailInbox.mail_id === action.id) {
          return {
            ...mailInbox,
            ...action.updates
          };
        } else {
          return mailInbox;
        };
    });
    default:
      return state;
  }
};
