// MailSents Reducer

const mailSentsReducerDefaultState = [];

export default (state = mailSentsReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_MAILSENT':
      return [
        ...state,
        action.mailSent
      ];
    case 'SET_MAILSENTS':
      return action.mailSents;
    default:
      return state;
  }
};