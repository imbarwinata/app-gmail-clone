export default (state = {}, action) => {
  switch (action.type) {
    case "LOGIN":
      return {
        auth: action.auth.auth,
        token: action.auth.token,
        adm: {
          id: action.auth.a_id,
          name: action.auth.a_name,
          email: action.auth.a_email
        }
      };
    case "LOGOUT":
      return {};
    default:
      return state;
  }
};
