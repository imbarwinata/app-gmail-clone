import React from 'react';
import { Router, Route, Switch, Link, NavLink } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import LoginPage from '../components/LoginPage';
import SignUpPage from '../components/SignUpPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import NotFoundPage from '../components/NotFoundPage';

// Mails module
import mailsComposePage from '../components/modules/mails/mailsComposePage';
import mailsInboxPage from '../components/modules/mails/mailsInboxPage';
import mailsSentPage from '../components/modules/mails/mailsSentPage';
import mailsInboxDetailPage from '../components/modules/mails/mailsInboxDetailPage';
import mailsSentDetailPage from '../components/modules/mails/mailsSentDetailPage';

import LogoutPage from '../components/LogoutPage';
import LoadingLogoutPage from '../components/layouts/LoadingLogoutPage';

export const history = createHistory();

const AppRouter = () => (
  <Router history={history}>
    <div>
    <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PublicRoute path="/prosesLogout" component={LoadingLogoutPage}/>
        <PublicRoute path="/signup" component={SignUpPage}/>
        <PrivateRoute path="/logout" component={LogoutPage} />
        
        {/* Mails Module */}
        <PrivateRoute path="/mails/compose" component={mailsComposePage} />
        <PrivateRoute path="/mails/inbox/:mail_id" component={mailsInboxDetailPage} />
        <PrivateRoute path="/mails/sent/:mail_id" component={mailsSentDetailPage} />
        <PrivateRoute path="/mails/inbox" component={mailsInboxPage} />
        <PrivateRoute path="/mails/sent" component={mailsSentPage} />

        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;