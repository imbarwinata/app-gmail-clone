import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import Header from "../components/layouts/Header";
import SubHeader from "../components/layouts/SubHeader";
import AppHeader from "../components/layouts/AppHeader";
import AppFooter from "../components/layouts/AppFooter";

const core = {
  titleHeader: "ini header",
  titleBody: "ini body",
  titleFooter: "Created By Imbar Winata"
};

export const PrivateRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? (
        <div>
          <div>
            <div className="m-grid m-grid--hor m-grid--root m-page">
              <AppHeader core={core} />
              <div className="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <button
                  className="m-aside-left-close m-aside-left-close--skin-dark"
                  id="m_aside_left_close_btn"
                >
                  <i className="la la-close" />
                </button>
                <Header {...props}/>
                <div className="m-grid__item m-grid__item--fluid m-wrapper">
                  {/* <SubHeader /> */}
                  <div className="m-content">
                    <div className="row">
                      <div className="col-xl-12">
                        <Component {...props} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <AppFooter core={core} />
            </div>
            <div
              className="m-scroll-top m-scroll-top--skin-top"
              data-toggle="m-scroll-top"
              data-scroll-offset="500"
              data-scroll-speed="300"
            >
              <i className="la la-arrow-up" />
            </div>
          </div>
        </div>
      ) : (
        <Redirect to="/" />
      )
    }
  />
);

const mapStateToProps = state => ({
  // isAuthenticated: !!state.auth.token //: get token in redux store
  isAuthenticated: !!localStorage.getItem('token') // get token in localStorage
});

export default connect(mapStateToProps)(PrivateRoute);
