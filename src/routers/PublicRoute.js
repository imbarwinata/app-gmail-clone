import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const core = {
  titleHeader: "ini header",
  titleBody: "ini body",
  titleFooter: "crated by Imbar Winata"
};

export const PublicRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => (
      isAuthenticated ? (
        <Redirect to="/mails/inbox" />
      ) : (
          <Component core={core} {...props} />
        )
    )} />
  );

const mapStateToProps = state => ({
  // isAuthenticated: !!state.auth.token //: get token in redux store
  isAuthenticated: !!localStorage.getItem('token') // get token in localStorage
});

export default connect(mapStateToProps)(PublicRoute);
