import React from "react";
import { connect } from "react-redux";
import { render } from "react-dom";
import { Link } from 'react-router-dom';
import { withFormik } from "formik";
import axios from "axios";
import Yup from "yup";
import uuid from 'uuid';
import md5 from 'md5';
import { startAddPerson } from './../actions/persons';
import { history } from "./../routers/AppRouter";

// CONFIG: Rules Schema, Value, Submit
const FormRules = withFormik({
  validationSchema: Yup.object().shape({
    name: Yup.string().required("Enter your name!"),
    hp: Yup.string().required("Enter your number phone!"),
    email: Yup.string().required("Enter your email!"),
    password: Yup.string().required("Enter your password!")
  }),
  mapPropsToValues: props => ({
    name: ``,
    hp: ``,
    email: ``,
    password: ``
  }),
  handleSubmit: (values, { setSubmitting, props }) => {
    setTimeout(() => {
      setSubmitting(false);
      if(values.gender == undefined){
        alert('Please choose your gender!');
      }
      else{
        axios.get(`${window.location.origin}/api/persons/check/${values.email}`).then(res => {
          if(res.data.status == 'success'){
            let p_id = uuid();
            let dataSignUp = {
              p_id,
              p_name: values.name,
              p_hp: values.hp,
              p_gender: values.gender ? values.gender:"m",
              p_email: values.email,
              p_pass: md5(values.password)
            };
            console.log('dataSignUp : ', dataSignUp);
            props.props.props.startAddPerson(dataSignUp);
            history.push("/");
          }
          else{
            alert(`${res.data.message}`);
          }
        })
        .catch(err => {});
      }
    }, 0);
  },
  displayName: "FormArea"
});

class FormArea extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      values,
      touched,
      dirty,
      errors,
      handleChange,
      handleBlur,
      handleSubmit,
      handleReset,
      isSubmitting
    } = this.props;
    return (
      <div className="m-grid m-grid--hor m-grid--root m-page">
        <div
          className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1"
          id="m_login"
          style={{ backgroundImage: "url(assets/app/media/img/bg/bg-1.jpg)" }}
        >
          <div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div className="m-login__container">
              <div className="m-login__logo">
                <a href="#">
                  <img src="./assets/app/media/img/logos/logo-1.png" style={{width: '125px'}} />
                </a>
              </div>
              <div className="m-login__signin">
                <div className="m-login__head">
                  <h3 className="m-login__title">
                    Sign Up Mail
                  </h3>
                </div>
                <form className="m-login__form m-form" onSubmit={handleSubmit}>
                    <div className="form-group m-form__group">
                        <input
                            type="text"
                            className="form-control m-input"
                            id="name"
                            placeholder="Enter your name"
                            value={values.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={
                                errors.name && touched.name
                                ? "form-control m-input form-control-feedback-error"
                                : "form-control m-input"
                            }
                        />
                        {errors.name &&
                        touched.name && (
                            <div className="input-feedback">{errors.name}</div>
                        )}
                    </div>

                   <div className="form-group m-form__group">
                    <input
                      type="text"
                      className="form-control m-input"
                      id="email"
                      placeholder="Enter your email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.email && touched.email
                          ? "form-control m-input form-control-feedback-error"
                          : "form-control m-input"
                      }
                    />
                    {errors.email &&
                      touched.email && (
                        <div className="input-feedback">{errors.email}</div>
                      )}
                  </div>
                  <br/>
                  <div className="form-group m-form__group" style={{paddingLeft: '135px'}}>
                    <label className="m-radio m-radio--state-success">
                        <input
                            id="male"
                            value={"m"}
                            name="gender"
                            type="radio"
                            onChange={handleChange}
                            checked={values.gender === 'm'}
                            label="Male"
                            onBlur={handleBlur}
                        /> Male
                        <span></span>
                    </label> &nbsp; &nbsp; 
                    <label className="m-radio m-radio--state-success">
                        <input
                            id="female"
                            value={"f"}
                            name="gender"
                            type="radio"
                            onChange={handleChange}
                            checked={values.gender === 'f'}
                            label="Female"
                            onBlur={handleBlur}
                        /> Female
                        <span></span>
                    </label>                         
                        {errors.email &&
                        touched.email && (
                            <div className="input-feedback">{errors.email}</div>
                        )}
                  </div>

                  <div className="form-group m-form__group">
                        <input
                        type="text"
                        className="form-control m-input"
                        id="hp"
                        placeholder="Enter your phone number"
                        value={values.hp}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                            errors.hp && touched.hp
                            ? "form-control m-input form-control-feedback-error"
                            : "form-control m-input"
                        }
                        />
                        {errors.hp &&
                        touched.hp && (
                            <div className="input-feedback">{errors.hp}</div>
                        )}
                  </div>

                  <div className="form-group m-form__group">
                    <input
                      type="password"
                      className="form-control m-input m-login__form-input--last"
                      id="password"
                      placeholder="Enter your password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.password && touched.password
                          ? "form-control m-input form-control-feedback-error"
                          : "form-control m-input"
                      }
                    />
                    {errors.password &&
                      touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                  </div>
                  <div className="m-login__form-action">
                    <button
                      type="submit"
                      id="m_login_signin_submit"
                      className="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary"
                    >
                      Sign Up
                    </button>
                  </div>

                  <div className="m-login__account">
                    <span className="m-login__account-msg">
                      Have an account yet ?
                    </span>
                    &nbsp;&nbsp;
                    <Link to="/" className="m-link m-link--light m-login__account-link">
                      Sign In
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const LoginFormRules = FormRules(FormArea);

const LoginForm = props => <LoginFormRules props={props} />;

export class SignUpPage extends React.Component {
  constructor(props){
    super(props);
  }
  render() {
    return <LoginForm props={this.props} />;
  }
}

const mapDispatchToProps = (dispatch) => ({
  startAddPerson: (dataSignUp) => dispatch(startAddPerson(dataSignUp))
});

export default connect(undefined, mapDispatchToProps)(SignUpPage);