import React from "react";

export class LoadingLogoutPage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    setTimeout(() => {
      this.props.history.push("/");
    }, 500);
    return (
      <div className="loader">
        <img className="loader__image" src="/images/smile-goodbye.gif" />
      </div>
    );
  }
}

export default LoadingLogoutPage;
