import React from 'react';

export class SubHeader extends React.Component{
    render(){
        return (
          <div className="m-subheader ">
            <div className="d-flex align-items-center">
              <div className="mr-auto">
                <h3 className="m-subheader__title ">
                  Dashboard
                </h3>
              </div>
            </div>
          </div>
        );
    }
}

export default SubHeader;