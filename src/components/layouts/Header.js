import React from "react";
import { Link, NavLink, IndexLink } from "react-router-dom";

export class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    let pathname = this.props.history.location.pathname;
    let menu = [
      {
        name: "Compose",
        link: "/mails/compose",
        icon: "m-menu__link-icon flaticon-paper-plane"
      },
      {
        name: "Inbox",
        link: "/mails/inbox",
        icon: "m-menu__link-icon flaticon-folder-2"
      },
      {
        name: "Sent",
        link: "/mails/sent",
        icon: "m-menu__link-icon flaticon-chat-1"
      },
      {
        name: "Draft",
        link: "#comingsoon",
        icon: "m-menu__link-icon flaticon-layers"
      }
    ];
    return (
      <div
        id="m_aside_left"
        className="m-grid__item m-aside-left m-aside-left--skin-dark"
      >
        <div
          id="m_ver_menu"
          className="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark"
          data-menu-vertical="true"
          data-menu-scrollable="false"
          data-menu-dropdown-timeout="500"
        >
          <ul className="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            {menu.map((item, index) => (
              <li
                className="m-menu__item  m-menu__item"
                aria-haspopup="true"
                key={index}
              >
                <Link
                  to={item.link}
                  className="m-menu__link"
                >
                  <i className={pathname.match(item.link) ? item.icon+' is-active':item.icon} />
                  <span className="m-menu__link-title">
                    <span className="m-menu__link-wrap">
                      <span className={pathname.match(item.link) ? 'm-menu__link-text is-active':'m-menu__link-text'}>{item.name}</span>
                    </span>
                  </span>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Header;
