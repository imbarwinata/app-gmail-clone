import React from "react";
import { Link, NavLink } from "react-router-dom";

export class AppHeader extends React.Component {
  render() {
    const backgroundStyle = {
      background:
        "url('" +
        __dirname +
        "assets/app/media/img/misc/user_profile_bg.jpg" +
        "')",
      backgroundSize: "cover"
    };
    return (
      <header
        className="m-grid__item m-header"
        data-minimize-offset="200"
        data-minimize-mobile-offset="200"
      >
        <div className="m-container m-container--fluid m-container--full-height">
          <div className="m-stack m-stack--ver m-stack--desktop">
            <div className="m-stack__item m-brand  m-brand--skin-dark ">
              <div className="m-stack m-stack--ver m-stack--general">
                <div className="m-stack__item m-stack__item--middle m-brand__logo">
                  <NavLink to="/mails/inbox" className="m-brand__logo-wrapper">
                    <img
                      style={{ width: "100%" }}
                      alt=""
                      src={
                        __dirname +
                        "assets/demo/default/media/img/logo/logo_default_dark.png"
                      }
                    />
                  </NavLink>
                </div>
                <div className="m-stack__item m-stack__item--middle m-brand__tools">
                  <a
                    href="javascript:;"
                    id="m_aside_left_minimize_toggle"
                    className="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block"
                  >
                    <span />
                  </a>
                  <a
                    href="javascript:;"
                    id="m_aside_left_offcanvas_toggle"
                    className="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block"
                  >
                    <span />
                  </a>
                  <a
                    id="m_aside_header_topbar_mobile_toggle"
                    href="javascript:;"
                    className="m-brand__icon m--visible-tablet-and-mobile-inline-block"
                  >
                    <i className="flaticon-more" />
                  </a>
                </div>
              </div>
            </div>
            <div
              className="m-stack__item m-stack__item--fluid m-header-head"
              id="m_header_nav"
            >
              <div
                id="m_header_topbar"
                className="m-topbar  m-stack m-stack--ver m-stack--general"
              >
                <div className="m-stack__item m-topbar__nav-wrapper">
                  <ul className="m-topbar__nav m-nav m-nav--inline">
                    <li
                      className="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                      data-dropdown-toggle="click"
                    >
                      <a href="#" className="m-nav__link m-dropdown__toggle">
                        <span className="m-topbar__userpic">
                          <img
                            src={
                              __dirname + "assets/app/media/img/users/user4.jpg"
                            }
                            className="m--img-rounded m--marginless m--img-centered"
                            alt=""
                          />
                        </span>
                        <span className="m-topbar__username m--hide">Nick</span>
                      </a>
                      <div className="m-dropdown__wrapper">
                        <span className="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" />
                        <div className="m-dropdown__inner">
                          <div
                            className="m-dropdown__header m--align-center"
                            style={backgroundStyle}
                          >
                            <div className="m-card-user m-card-user--skin-dark">
                              <div className="m-card-user__pic">
                                <img
                                  src={
                                    __dirname +
                                    "assets/app/media/img/users/user4.jpg"
                                  }
                                  className="m--img-rounded m--marginless"
                                  alt=""
                                />
                              </div>
                              <div className="m-card-user__details">
                                <span className="m-card-user__name m--font-weight-500">
                                  User Email
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className="m-dropdown__body">
                            <div className="m-dropdown__content">
                              <ul className="m-nav m-nav--skin-light">
                                <li className="m-nav__section m--hide">
                                  <span className="m-nav__section-text">
                                    Section
                                  </span>
                                </li>
                                <li className="pull-right m-nav__item">
                                  <NavLink
                                    to="/logout"
                                    className="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder"
                                  >
                                    Logout
                                  </NavLink>
                                </li>
                                <br />
                                <br />
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default AppHeader;