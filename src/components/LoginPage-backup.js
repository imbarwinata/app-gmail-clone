import React from "react";
import { connect } from "react-redux";
import { render } from "react-dom";
import { Link } from 'react-router-dom';
import { withFormik } from "formik";
import Yup from "yup";
import md5 from 'md5';
import { startLogin } from './../actions/auth';

// CONFIG: Rules Schema, Value, Submit
const FormRules = withFormik({
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required!"),
    password: Yup.string().required("Password is required!")
  }),
  mapPropsToValues: props => ({
    email: ``,
    password: ``
  }),
  handleSubmit: (values, { setSubmitting, props }) => {
    setTimeout(() => {
      setSubmitting(false);
      let dataLogin = {
        email: values.email,
        password: md5(values.password)
      };
      props.props.props.startLogin(dataLogin);
    }, 0);
  },
  displayName: "FormArea"
});

class FormArea extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      values,
      touched,
      dirty,
      errors,
      handleChange,
      handleBlur,
      handleSubmit,
      handleReset,
      isSubmitting
    } = this.props;
    return (
      <div className="m-grid m-grid--hor m-grid--root m-page">
        <div
          className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1"
          id="m_login"
          style={{ backgroundImage: "url(assets/app/media/img/bg/bg-1.jpg)" }}
        >
          <div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
            <div className="m-login__container">
              <div className="m-login__logo">
                <a href="#">
                  <img src="./assets/app/media/img/logos/logo-1.png" />
                </a>
              </div>
              <div className="m-login__signin">
                <div class="m-login__head">
                  <h3 class="m-login__title">
                    Sign In To Admin
                  </h3>
                </div>
                <form className="m-login__form m-form" onSubmit={handleSubmit}>
                  <div className="form-group m-form__group">
                    <input
                      type="text"
                      className="form-control m-input"
                      id="email"
                      placeholder="Enter email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.email && touched.email
                          ? "form-control m-input form-control-feedback-error"
                          : "form-control m-input"
                      }
                    />
                    {errors.email &&
                      touched.email && (
                        <div className="input-feedback">{errors.email}</div>
                      )}
                  </div>
                  <div className="form-group m-form__group">
                    <input
                      type="password"
                      className="form-control m-input m-login__form-input--last"
                      id="password"
                      placeholder="Enter password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        errors.password && touched.password
                          ? "form-control m-input form-control-feedback-error"
                          : "form-control m-input"
                      }
                    />
                    {errors.password &&
                      touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                  </div>
                  <div className="row m-login__form-sub">
                    <div className="col m--align-left m-login__form-left">
                      <label className="m-checkbox  m-checkbox--light">
                        <input type="checkbox" name="remember" />
                        Remember me
                        <span />
                      </label>
                    </div>
                    <div className="col m--align-right m-login__form-right">
                      <label className="m-checkbox  m-checkbox--light">
                        <Link to="/help" style={{ color: "white" }}>Help</Link>
                      </label>
                    </div>
                  </div>
                  <div className="m-login__form-action">
                    <button
                      type="submit"
                      id="m_login_signin_submit"
                      className="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary"
                    >
                      Sign In
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const LoginFormRules = FormRules(FormArea);

const LoginForm = props => <LoginFormRules props={props} />;

export class LoginPage extends React.Component {
  constructor(props){
    super(props);
  }
  render() {
    return <LoginForm props={this.props} />;
  }
}

const mapDispatchToProps = (dispatch) => ({
  startLogin: (dataLogin) => dispatch(startLogin(dataLogin))
});

export default connect(undefined, mapDispatchToProps)(LoginPage);