import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { startLogout } from './../actions/auth';

export class LogoutPage extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        this.props.startLogout();
        this.props.history.push('/prosesLogout');
        
        return (
            <div>Logout Loh</div>
        );
    }
}

const mapDispatchToProps = (dispatch, props) => ({
    startLogout: () => dispatch(startLogout())
});
  
export default connect(undefined, mapDispatchToProps)(LogoutPage);