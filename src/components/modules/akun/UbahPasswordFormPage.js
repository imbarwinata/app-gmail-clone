import React from "react";
import md5 from "md5";

export default class UbahPasswordFormPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      password_konfir: "",
      password_last: "",
      error_password: "", 
      error_password_konfir: "",
      error_password_last: ""
    };
  }
  // PART: handleChange
  onPasswordChange = e => {
    const password = e.target.value;
    this.setState(() => ({ password }));
  };
  onPasswordKonfirChange = e => {
    const password_konfir = e.target.value;
    this.setState(() => ({ password_konfir }));
  };
  onPasswordLastChange = e => {
    const password_last = e.target.value;
    this.setState(() => ({ password_last }));
  };

  // PART: handleSubmit
  onSubmit = e => {
    e.preventDefault();

    if (!this.state.password) {
      this.setState(() => ({
        error_password: "Silahkan isi password!"
      }));
    } else if (!this.state.password_konfir) {
      this.setState(() => ({
        error_password_konfir: "Silahkan isi konfirmasi password!"
      }));
    } else if (!this.state.password_last) {
      this.setState(() => ({
        error_password_last: "Silahkan isi password lama!"
      }));
    } else if (this.state.password_konfir != this.state.password) {
      this.setState(() => ({
        error_password: "Konfirmasi password tidak sama!"
      }));
    } else {
      this.setState(() => ({
          error_password: "", error_password_konfir: "", error_password_last: ""
      }));
      let dataAdmin = {
        adm_id: this.props.adm.id,
        adm_namapengguna: this.props.adm.username,
        adm_password: md5(this.state.password_last),
        adm_passwordNew: md5(this.state.password)
      };
      this.props.onSubmit(dataAdmin);
    }
  };
  render() {
    return (
      <form
        onSubmit={this.onSubmit}
        className="m-form m-form--fit m-form--label-align-right"
      >
        <div className="m-portlet__body" style={{ paddingTop: "0px" }}>
          <div
            className={
              this.state.error_password ? "form-group m-form__group has-danger" : "form-group m-form__group"
            }>
            <label htmlFor="password">Password Baru</label>
            <input
              type="password"
              className={
                this.state.error_password ? 
                  "form-control m-input form-control-feedback-error" : "form-control m-input"
              }
              id="password"
              placeholder="Masukkan Password Baru Anda"
              value={this.state.password}
              onChange={this.onPasswordChange}
            />
            {this.state.error_password ? (
              <span className="input-feedback">{this.state.error_password}</span>
            ) : (
              ""
            )}
          </div>

          <div
            className={
              this.state.error_password_konfir ? "form-group m-form__group has-danger" : "form-group m-form__group"
            }>
            <label htmlFor="password_konfir">Konfirmasi Password Baru</label>
            <input
              type="password"
              className={
                this.state.error_password_konfir ? 
                  "form-control m-input form-control-feedback-error" : "form-control m-input"
              }
              id="password_konfir"
              placeholder="Masukkan Konfirmasi Password Baru Anda"
              value={this.state.password_konfir}
              onChange={this.onPasswordKonfirChange}
            />
            {this.state.error_password_konfir ? (
              <span className="input-feedback">{this.state.error_password_konfir}</span>
            ) : (
              ""
            )}
          </div>
          
          <hr/>
          <div
            className={
              this.state.error_password_last ? "form-group m-form__group has-danger" : "form-group m-form__group"
            }>
            <label htmlFor="password_last">Password Lama</label>
            <input
              type="password"
              className={
                this.state.error_password_last ? 
                  "form-control m-input form-control-feedback-error" : "form-control m-input"
              }
              id="password_last"
              placeholder="Masukkan Password Lama Anda"
              value={this.state.password_last}
              onChange={this.onPasswordLastChange}
            />
            {this.state.error_password_last ? (
              <span className="input-feedback">{this.state.error_password_last}</span>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="m-portlet__foot m-portlet__foot--fit">
          <div className="m-form__actions">
            <button type="submit" className="btn btn-primary pull-right">
              Simpan
            </button>
            <button
              type="reset"
              className="btn btn-secondary pull-right"
              style={{ marginRight: "5px" }}
            >
              Batal
            </button>
            &nbsp;
          </div>
        </div>
      </form>
    );
  }
}