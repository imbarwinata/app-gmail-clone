import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { startChangeDataPasswordAdmin } from './../../../actions/auth';
import UbahPasswordFormPage from './UbahPasswordFormPage';

export class UbahPasswordPage extends React.Component {
    constructor(props){
        super(props);
    }
    onSubmit = (dataAdmin) => {
        this.props.startChangeDataPasswordAdmin(dataAdmin);
    };
    render() {
        return (
            <div className="m-portlet m-portlet--full-height">
                <div className="m-portlet__head">
                    <div className="m-portlet__head-caption">
                        <div className="m-portlet__head-title" style={{width: '100%'}}>
                            <h3 className="m-portlet__head-text">
                                Ubah Password
                            </h3>
                        </div>
                    </div>
                </div>
                <div className="m-portlet__body">
                    <UbahPasswordFormPage
                        adm={this.props.adm}
                        onSubmit={this.onSubmit}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    adm: state.auth.adm
  });
const mapDispatchToProps = (dispatch, props) => ({
    startChangeDataPasswordAdmin: (dataAdmin) => dispatch(startChangeDataPasswordAdmin(dataAdmin))
});
export default connect(mapStateToProps, mapDispatchToProps)(UbahPasswordPage);