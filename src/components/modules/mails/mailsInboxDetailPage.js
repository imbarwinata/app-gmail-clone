import React from "react";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { startEditMailInbox } from '../../../actions/mailInboxs';

export class mailsInboxDetailPage extends React.Component {
  constructor(props){
    super(props);
    props.startEditMailInbox(props.mail_id ? props.mail_id: "", { mail_read: "t" });

    this.state = {
        mail_from: this.props.mailInbox ? this.props.mailInbox.mail_from : "",
        mail_subject: this.props.mailInbox ? this.props.mailInbox.mail_subject : "",
        mail_message: this.props.mailInbox ? this.props.mailInbox.mail_message : ""
    }
  }
  render() {
    return (
      <div className="m-portlet m-portlet--full-height">
        <div className="m-portlet__head">
          <div className="m-portlet__head-caption">
            <div className="m-portlet__head-title" style={{ width: "100%" }}>
              <h3 className="m-portlet__head-text">
                Detail Mail Inbox
                <div className="pull-right">
                  <Link to={`/mails/inbox`} className="b-add-link">
                    Back To Inbox &nbsp;{" "}
                    <i className="m-menu__link-icon flaticon-list" />
                  </Link>
                </div>
              </h3>
            </div>
          </div>
        </div>
        <div className="m-portlet__body">
          <form className="m-form m-form--fit m-form--label-align-right">
            <div className="m-portlet__body" style={{ paddingTop: "0px" }}>
                
                <div className="form-group m-form__group">
                    <label htmlFor="mail_subject">From</label>
                    <input
                    type="text"
                    className="form-control m-input"
                    id="mail_subject"
                    placeholder="Enter mail_subject"
                    value={this.state.mail_from}
                    disabled="disabled"
                    />
                </div>

              <div className="form-group m-form__group">
                <label htmlFor="mail_subject">Subject</label>
                <input
                  type="text"
                  className="form-control m-input"
                  id="mail_subject"
                  placeholder="Enter mail_subject"
                  value={this.state.mail_subject}
                  disabled="disabled"
                />
              </div>

              <div className="form-group m-form__group">
                <label htmlFor="mail_message">Message</label>
                <div style={{border: "1px solid #ebedf2", borderRadius: "5px", padding: "20px"}} dangerouslySetInnerHTML={{ __html: this.state.mail_message }} />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  mail_id: props.match.params.mail_id ? props.match.params.mail_id: "",
  mailInbox: state.mailInboxs.find(mailInboxs => mailInboxs.mail_id === props.match.params.mail_id)
});
const mapDispatchToProps = (dispatch, props) => ({
  startEditMailInbox: (id, updates) => dispatch(startEditMailInbox(id, updates))
});
export default connect(mapStateToProps, mapDispatchToProps)(mailsInboxDetailPage);