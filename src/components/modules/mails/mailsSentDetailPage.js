import React from "react";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";

export class mailsSentDetailPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        mail_to: this.props.mailSent ? this.props.mailSent.mail_to : "",
        mail_subject: this.props.mailSent ? this.props.mailSent.mail_subject : "",
        mail_message: this.props.mailSent ? this.props.mailSent.mail_message : ""
    }
  }
  render() {
    return (
      <div className="m-portlet m-portlet--full-height">
        <div className="m-portlet__head">
          <div className="m-portlet__head-caption">
            <div className="m-portlet__head-title" style={{ width: "100%" }}>
              <h3 className="m-portlet__head-text">
                Detail Mail Sent
                <div className="pull-right">
                  <Link to={`/mails/sent`} className="b-add-link">
                    Back To Sent &nbsp;{" "}
                    <i className="m-menu__link-icon flaticon-list" />
                  </Link>
                </div>
              </h3>
            </div>
          </div>
        </div>
        <div className="m-portlet__body">
          <form className="m-form m-form--fit m-form--label-align-right">
            <div className="m-portlet__body" style={{ paddingTop: "0px" }}>
                
                <div className="form-group m-form__group">
                    <label htmlFor="mail_subject">To</label>
                    <input
                    type="text"
                    className="form-control m-input"
                    id="mail_subject"
                    placeholder="Enter mail_subject"
                    value={this.state.mail_to}
                    disabled="disabled"
                    />
                </div>

              <div className="form-group m-form__group">
                <label htmlFor="mail_subject">Subject</label>
                <input
                  type="text"
                  className="form-control m-input"
                  id="mail_subject"
                  placeholder="Enter mail_subject"
                  value={this.state.mail_subject}
                  disabled="disabled"
                />
              </div>

              <div className="form-group m-form__group">
                <label htmlFor="mail_message">Message</label>
                <div style={{border: "1px solid #ebedf2", borderRadius: "5px", padding: "20px"}} dangerouslySetInnerHTML={{ __html: this.state.mail_message }} />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  mailSent: state.mailSents.find(mailSents => mailSents.mail_id === props.match.params.mail_id)
});
export default connect(mapStateToProps)(mailsSentDetailPage);