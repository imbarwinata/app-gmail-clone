import React from "react";
import { connect } from "react-redux";
import { render } from "react-dom";
import { Link } from 'react-router-dom';
import axios from 'axios';
import uuid from 'uuid';
import { Layout, Form, Input, Button, notification, Icon } from 'antd';
import { Grid, Row, Col } from 'react-flexbox-grid';
const { Content, Footer } = Layout;
const FormItem = Form.Item;
import CKEditor from "react-ckeditor-component";
import { config } from "./../../../helpers/AppConfig";
import { startAddMailSent } from './../../../actions/mailSents';
import { history } from "./../../../routers/AppRouter";
import 'antd/dist/antd.css';
import { log } from "util";

export class mailsComposeFormPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          mail_to: "", mail_subject: "", "mail_message":"", mail_read: "f"
        };
        this.onMessageChange = this.onMessageChange.bind(this);
    }
    // NOTE:: handle change input
    onEmailChange = value => {
      let mail_to = value;
      this.setState(() => ({ mail_to }));
    };
    onSubjectChange = value => {
      let mail_subject = value;
      this.setState(() => ({ mail_subject }));
    };
    onMessageChange = e => {
      const mail_message = e.editor.getData();
      this.setState(() => ({ mail_message }));
    };
    // NOTE:: handle when form submit
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          let mail_id = uuid();
          let dataCompose = {
            mail_id,
            mail_from: this.props.email,
            mail_to: values.email,
            mail_subject: values.subject,
            mail_message: this.state.mail_message,
            mail_read: this.state.mail_read
          };
          this.props.startAddMailSent(dataCompose);
          notification.open({
            message: 'Successfully',
            description: 'Email successfully submitted.',
            icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
          });
          history.push("/mails/sent");
        }
      });
    }
    validateSubject = (rule, value, callback) => {
      if (value && value.length > 78 ) {
        callback('Ops, the number of subject characters exceeds the limit(78)!');
      } else { callback(); }
    }
    validateEmail = (rule, value, callback) => {
      const form = this.props.form;
      if(value && value.length > 254){
        callback('Ops, the number of email characters exceeds the limit(254) !');
      } else{
        axios.get(`${window.location.origin}/api/mails/check-exist/${value}`).then(res => {
          if(res.data.status == "success"){
            callback();
          }
          else{
            callback(res.data.message);
          }
        }).catch((err) => {});
      }
    }
    render() {
      const formItemLayout = {
        labelCol:   { xs: { span: 24 }, sm: { span: 24 } },
        wrapperCol: { xs: { span: 24 }, sm: { span: 24 } },
      };
      const tailFormItemLayout = {
        wrapperCol: { xs: { span: 24, offset: 0, }, sm: { span: 24, offset: 0, },
        },
      };
      const { getFieldDecorator } = this.props.form;

      return (
        <div className="m-portlet m-portlet--full-height" id="head">
        <div className="m-portlet__head">
          <div className="m-portlet__head-caption">
            <div className="m-portlet__head-title" style={{ width: "100%" }}>
              <h3 className="m-portlet__head-text">
                Compose Mail
                <div className="pull-right">
                  <Link to={`/mails/sent`} className="b-add-link">
                    Go To Sent &nbsp;{" "}
                    <i className="m-menu__link-icon flaticon-chat-1" />
                  </Link>
                </div>
              </h3>
            </div>
          </div>
        </div>
        <div className="m-portlet__body">
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...formItemLayout} label="To">
            {getFieldDecorator('email', {
              rules: [{
                required: true, message: 'Please enter your destination email!'
              }, {
                validator: this.validateEmail
              }],
            })(
              <Input placeholder="Enter your destination email ..." onChange={this.onSubjectChange} />
            )}
          </FormItem>

          <FormItem {...formItemLayout} label="Subject">
            {getFieldDecorator('subject', {
              rules: [{
                required: true, message: 'Please enter your subject!'
              }, {
                validator: this.validateSubject
              }],
            })(
              <Input placeholder="Enter your subject ..." onChange={this.onSubjectChange} />
            )}
          </FormItem>

          <FormItem {...formItemLayout} label="Message" className="message-compose">
            {getFieldDecorator('message', {
              rules: [{
                required: false, message: 'Please enter your message!'
              }],
            })(
              <CKEditor 
                activeClass="editor" 
                content={this.state.mail_message != "" ? this.state.mail_message : ""} 
                events={{
                  "change": this.onMessageChange
                }}
              />
            )}
          </FormItem>
          
          <FormItem {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
                Kirim
            </Button>
          </FormItem>
        </Form>  

        </div>
      </div>
      );
    }
}

const mailsComposePage = Form.create()(mailsComposeFormPage);

const mapStateToProps = (state) => {
  return {
      email: state.auth.adm ? state.auth.adm.email:""
  };
};
const mapDispatchToProps = (dispatch, props) => ({
  startAddMailSent: (dataCompose) => dispatch(startAddMailSent(dataCompose))    
});
export default connect(mapStateToProps, mapDispatchToProps)(mailsComposePage);