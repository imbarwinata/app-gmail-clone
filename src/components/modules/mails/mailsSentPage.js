var _ = require('lodash');
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { startSetMailSents } from '../../../actions/mailSents';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import humanFormat from 'human-format';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

export class mailsSentPage extends React.Component {
    constructor(props) {
        props.startSetMailSents(props.email ? props.email: "");
        super(props);
    }
    onRefreshPage = () => {
        this.props.startSetMailSents(props.email ? props.email: "");
    }
    remote(remoteObj) {
        remoteObj.cellEdit = false;
        remoteObj.dropRow = false;
        return remoteObj;
    }
    
    render() {
        const cellEditProp = { mode: 'dbclick' };
        const selectRow = {
            mode: 'checkbox',
            cliclToSelct: true,
            showOnlySelected: true
        };
        const options = {
            prePage: 'Prev',
            nextPage: 'Next',
            firstPage: 'First',
            lastPage: 'Last',
            paginationShowsTotal: this.renderShowsTotal,
            defaultSortName: 'createdAt'
        };
        const actionFormatter = (cell, row) => {            
            return (
                <div>
                    <Link to={`/mails/sent/${cell}`} title="Show Inbox">
                        <i className="m-menu__link-icon flaticon-information"></i>
                    </Link>
                </div>
            );
        }
        let mailSents = _.orderBy(this.props.mailSents ? this.props.mailSents:[], ['createdAt'], ['desc']);

        return (
            <div className="m-portlet m-portlet--full-height">
                <div className="m-portlet__head">
                <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title" style={{width: '100%'}}>
                        <h3 className="m-portlet__head-text">
                            Sent
                            <div className="pull-right">
                                <button onClick={this.onRefreshPage} className="button-to-link">
                                    <i className="m-m-menu__link-icon flaticon-refresh"></i> &nbsp; 
                                </button> &nbsp;| &nbsp; 
                                <Link to={`/mails/compose`} className="b-add-link">
                                    &nbsp; <i className="m-menu__link-icon flaticon-paper-plane"></i>
                                </Link>
                            </div>
                        </h3>
                    </div>
                </div>
                </div>
                <div className="m-portlet__body">
                    <BootstrapTable striped hover responsive
                        data={mailSents} 
                        pagination={true}
                        options={options} 
                        remote={ this.remote }
                        selectRow={ selectRow }
                        cellEdit={ cellEditProp }
                        search
                        keyField='mail_id'>
                        
                        <TableHeaderColumn 
                            dataField='mail_to'
                            editable={ false }
                            dataSort={ true }
                            filter={ { type: 'TextFilter', delay: 0 } }
                        >To</TableHeaderColumn>
                        <TableHeaderColumn 
                            dataField='mail_subject'
                            editable={ false }
                            dataSort={ true }
                            filter={ { type: 'TextFilter', delay: 0 } }
                        >Subject</TableHeaderColumn>
                        <TableHeaderColumn 
                            dataField='mail_id' 
                            dataFormat={ actionFormatter }
                            width='90'
                            dataAlign='center'
                        >Action
                        </TableHeaderColumn>
                    </BootstrapTable>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        mailSents: state.mailSents,
        email: state.auth.adm ? state.auth.adm.email:""
    };
};

const mapDispatchToProps = (dispatch, props) => ({
    startSetMailSents: (email) => dispatch(startSetMailSents(email))
});
  
export default connect(mapStateToProps, mapDispatchToProps)(mailsSentPage);