import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
  <div className="m-grid m-grid--hor m-grid--root m-page" style={ { height: '100vh' } }>
    <div className="m-grid__item m-grid__item--fluid m-grid  m-error-1" style={ { backgroundImage: "url('" + __dirname + 'assets/app/media/img/error/bg1.jpg' + "')"} }>
      <div className="m-error_container">
        <span className="m-error_number">
          <h1>
            404
          </h1>
        </span>
        <p className="m-error_desc">
          OOPS! Something went wrong here
        </p>
      </div>
    </div>
  </div>
);

export default NotFoundPage;
