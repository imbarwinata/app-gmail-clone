import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import AppRouter, { history } from "./routers/AppRouter";
import LoadingPage from "./components/layouts/LoadingPage";
import configureStore from "./store/configureStore";
import { loadLink, loadScript } from "./helpers/AppLoader";
import { startSetLogin, startLogout } from "./actions/auth";
import "normalize.css/normalize.css";
import "./styles/styles.scss";
import { setTimeout, log } from "core-js";
import uuid from "uuid";
import { getSession } from "./helpers/AppConfig";
const token = getSession.backoffice.token;

const store = configureStore();

const container = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(<LoadingPage />, document.getElementById("app"));

let hasRendered = false;
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(container, document.getElementById("app"));
    hasRendered = true;
  }
};

setTimeout(() => {
  setTimeout(() => {
    if (token != null) {
      store.dispatch(startLogout());
      renderApp();
      history.push("/");
    } else {
      renderApp();
      // history.push("/");
    }
  }, 2000);

  loadLink(
    "link",
    "stylesheet",
    "text/css",
    __dirname + "assets/vendors/base/vendors.bundle.css"
  );
  loadLink(
    "link",
    "stylesheet",
    "text/css",
    __dirname + "assets/demo/default/base/style.bundle.css"
  );
  loadLink(
    "link",
    "shortcut icon",
    "",
    __dirname + "assets/demo/default/media/img/logo/favicon.ico"
  );
  loadScript("scripts", __dirname + "assets/vendors/base/vendors.bundle.js");
  loadScript(
    "scripts",
    __dirname + "assets/demo/default/base/scripts.bundle.js"
  );
  loadScript("scripts", __dirname + "assets/app/js/dashboard.js");
}, 0); // 5500
