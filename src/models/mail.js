'use strict';
module.exports = (sequelize, DataTypes) => {
  var Mail = sequelize.define('Mail', {
    mail_id: DataTypes.STRING(40),
    mail_subject: DataTypes.STRING(78),
    mail_from: DataTypes.STRING(254),
    mail_to: DataTypes.STRING(254),
    mail_message: DataTypes.TEXT,
    mail_read: DataTypes.STRING(1)
  }, {});
  Mail.associate = function(models) {
    // associations can be defined here
  };
  return Mail;
};