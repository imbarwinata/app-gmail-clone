'use strict';
module.exports = (sequelize, DataTypes) => {
  var Person = sequelize.define('Person', {
    p_id: DataTypes.STRING(40),
    p_name: DataTypes.STRING(50),
    p_gender: DataTypes.STRING(1),
    p_email: DataTypes.STRING(40),
    p_pass: DataTypes.STRING(40),
    p_hp: DataTypes.STRING(13),
    p_aktif: DataTypes.STRING(1)
  }, {});
  Person.associate = function(models) {
    // associations can be defined here
  };
  return Person;
};